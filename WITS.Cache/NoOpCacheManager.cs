﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WITS.Cache
{
    public class NoOpCacheManager : ICacheManager
    {
        public Task<int> Clear()
        {
            return Task.FromResult(0);
        }

        public Task<object> Get(string key)
        {
            return Task.FromResult((object) null);
        }

        public Task<TOut> Get<TOut>(string key)
        {
            return Task.FromResult(default(TOut));
        }

        public Task<HashSet<string>> Keys()
        {
            return Task.FromResult(new HashSet<string>());
        }

        public Task Remove(string key)
        {
            return Task.CompletedTask;
        }

        public Task<object> Set(string key, object value)
        {
            return Task.FromResult(value);
        }

        public Task<object> Set(string key, object value, TimeSpan slidingExpiration)
        {
            return Task.FromResult(value);
        }

        public Task<object> Set(string key, object value, DateTimeOffset absoluteExpiration)
        {
            return Task.FromResult(value);
        }

        public Task<TItem> Set<TItem>(string key, TItem value)
        {
            return Task.FromResult(value);
        }

        public Task<TItem> Set<TItem>(string key, TItem value, TimeSpan slidingExpiration)
        {
            return Task.FromResult(value);
        }

        public Task<TItem> Set<TItem>(string key, TItem value, DateTimeOffset absoluteExpiration)
        {
            return Task.FromResult(value);
        }
    }
}
