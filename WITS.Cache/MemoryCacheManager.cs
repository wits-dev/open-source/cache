﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WITS.Cache
{
    public class MemoryCacheManager : ICacheManager, IDisposable
    {
        private const string KeysKey = "__wits_cache_internal_cache_keys";
        private readonly IMemoryCache _manager;
        private readonly ReaderWriterLockSlim _entryLock;

        public MemoryCacheManager(IMemoryCache memoryCache)
        {
            _manager = memoryCache;
            _entryLock = new ReaderWriterLockSlim();
        }

        public Task<object> Get(string key)
        {
            _entryLock.EnterReadLock();
            try
            {
                return Task.FromResult(GetInternal<object>(key));
            }
            finally
            {
                _entryLock.ExitReadLock();
            }
        }

        public Task<TOut> Get<TOut>(string key)
        {
            _entryLock.EnterReadLock();
            try
            {
                return Task.FromResult(GetInternal<TOut>(key));
            }
            finally
            {
                _entryLock.ExitReadLock();
            }
        }


        public Task<object> Set(string key, object value)
        {
            _entryLock.EnterWriteLock();
            try
            {
                AddKeyInternal(key);
                return Task.FromResult(_manager.Set(key, value));
            }
            finally
            {
                _entryLock.ExitWriteLock();
            }
        }

        public Task<object> Set(string key, object value, TimeSpan slidingExpiration)
        {
            _entryLock.EnterWriteLock();
            try
            {
                AddKeyInternal(key);
                return Task.FromResult(_manager.Set(key, value, slidingExpiration));
            }
            finally
            {
                _entryLock.ExitWriteLock();
            }
        }

        public Task<object> Set(string key, object value, DateTimeOffset absoluteExpiration)
        {
            _entryLock.EnterWriteLock();
            try
            {
                AddKeyInternal(key);
                return Task.FromResult(_manager.Set(key, value, absoluteExpiration));
            }
            finally
            {
                _entryLock.ExitWriteLock();
            }
        }

        public Task<TItem> Set<TItem>(string key, TItem value)
        {
            _entryLock.EnterWriteLock();
            try
            {
                AddKeyInternal(key);
                return Task.FromResult(_manager.Set(key, value));
            }
            finally
            {
                _entryLock.ExitWriteLock();
            }
        }

        public Task<TItem> Set<TItem>(string key, TItem value, TimeSpan slidingExpiration)
        {
            _entryLock.EnterWriteLock();
            try
            {
                AddKeyInternal(key);
                return Task.FromResult(_manager.Set(key, value, slidingExpiration));
            }
            finally
            {
                _entryLock.ExitWriteLock();
            }
        }

        public Task<TItem> Set<TItem>(string key, TItem value, DateTimeOffset absoluteExpiration)
        {
            _entryLock.EnterWriteLock();
            try
            {
                AddKeyInternal(key);
                return Task.FromResult(_manager.Set(key, value, absoluteExpiration));
            }
            finally
            {
                _entryLock.ExitWriteLock();
            }
        }

        public Task Remove(string key)
        {
            _entryLock.EnterWriteLock();
            try
            {
                RemoveKeyInternal(key);
            }
            finally
            {
                _entryLock.ExitWriteLock();
            }

            return Task.CompletedTask;
        }

        public Task<int> Clear()
        {
            _entryLock.EnterWriteLock();
            try
            {
                var keys = new List<string>(KeysInternal());
                foreach (var key in keys)
                {
                    RemoveKeyInternal(key);
                }

                return Task.FromResult(keys.Count);
            }
            finally
            {
                _entryLock.ExitWriteLock();
            }
        }
        public Task<HashSet<string>> Keys()
        {
            _entryLock.EnterReadLock();
            try
            {
                return Task.FromResult(KeysInternal());
            }
            finally
            {
                _entryLock.ExitReadLock();
            }
        }

        protected void AddKeyInternal(string key)
        {
            var keys = KeysInternal();
            keys.Add(key);

            _manager.Set(KeysKey, keys);
        }

        protected TOut GetInternal<TOut>(string key)
        {
            return _manager.Get<TOut>(key);
        }

        protected void RemoveKeyInternal(string key)
        {
            var keys = KeysInternal();

            if (keys != null)
            {
                keys.Remove(key);
                _manager.Set(KeysKey, keys);
            }
            _manager.Remove(key);
        }

        protected HashSet<string> KeysInternal()
        {
            return _manager.Get<HashSet<string>>(KeysKey) ?? new HashSet<string>();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _manager.Dispose();
                }

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~PetraCacheManager() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
