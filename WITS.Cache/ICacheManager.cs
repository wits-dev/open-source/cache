﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WITS.Cache
{
    public interface ICacheManager
    {
        Task<object> Get(string key);

        Task<TOut> Get<TOut>(string key);

        Task<object> Set(string key, object value);

        Task<object> Set(string key, object value, TimeSpan slidingExpiration);

        Task<object> Set(string key, object value, DateTimeOffset absoluteExpiration);

        Task<TItem> Set<TItem>(string key, TItem value);

        Task<TItem> Set<TItem>(string key, TItem value, TimeSpan slidingExpiration);

        Task<TItem> Set<TItem>(string key, TItem value, DateTimeOffset absoluteExpiration);

        Task<HashSet<string>> Keys();

        Task Remove(string key);
        Task<int> Clear();
    }
}
